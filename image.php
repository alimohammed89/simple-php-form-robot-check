<?php



function draw() {
	$pattern = "ABCDEFGHJKLMNPQRSTVWXYZ1234567890";
	header("Content-Type: image/jpg");
    $im = imagecreatetruecolor(200, 60);
    $background_color = imagecolorallocate($im,0, 0, 0);
    imagefill($im,0,0,$background_color);
    $text_color = imagecolorallocate($im, 233, 14, 91);
    $numOfChars = 6;
    $rotateAngle = array(-20,20);
    for($i=0;$i<6;$i++) {
        $tmpim = imagecreatetruecolor(40, 40);
        $background_color2 = imagecolorallocate($tmpim, 0, 255, 0);
        $white = imagecolorallocate($tmpim, 255, 255, 255);
        $black = imagecolorallocate($tmpim, 0, 0, 0);
        $text_color2 = imagecolorallocate($tmpim, 233, 14, 91);
        imagefilledrectangle($tmpim, 0, 0, 39, 39, $black);
        $r = rand(0, strlen($pattern) - 1);
        $an = rand($rotateAngle[0],$rotateAngle[1]);
        // You can put more than one ttf file (I'm using 3 fonts with 1.ttf, 2.ttf, 3.ttf as filename)
        imagettftext($tmpim, 14, $an, 10,20, $text_color2,"path/to/ttf/".rand(1,3).".ttf",$pattern[$r]);
        imagecopy($im, $tmpim, ($i+1) * 25, rand(3,30), 0, 0, 40, 40);
    }
    imagejpeg($im,null,100);
    imagedestroy($im);
}

draw();